package sam7ibrahim.cachutask.com.data.models;

/**
 * Created by Same7ibrahim on 15/05/2017.
 */

public interface OnResult<T> {
    void onSuccess(T data);

    void onFailure(Throwable throwable);

    void onFailure(int code, String message);
}
