package sam7ibrahim.cachutask.com.data.models;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import sam7ibrahim.cachutask.com.BuildConfig;
import sam7ibrahim.cachutask.com.data.models.dao.RepositoryDao;

@Database(entities = {SingleRepository.class, Owner.class}, version = BuildConfig.VERSION_CODE, exportSchema = false)
public abstract class RepositoriesDataBase extends RoomDatabase {

    public abstract RepositoryDao repositoryDao();


}
