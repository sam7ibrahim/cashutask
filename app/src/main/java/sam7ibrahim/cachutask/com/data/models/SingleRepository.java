package sam7ibrahim.cachutask.com.data.models;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.parceler.Parcel;

import java.io.Serializable;

@Entity(tableName = "singleRepository")
@Parcel
public class SingleRepository implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id", index = true)
    private int id;

    @ColumnInfo(name = "nodeId")
    private String node_id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "fullName")

    private String full_name;

    @ColumnInfo(name = "private")
    private boolean private_;

    @ColumnInfo(name = "description")
    private String description;

    @Embedded
    private Owner owner;

    public SingleRepository(Owner owner, int id, String node_id, String name, String full_name, boolean private_, String description) {
        this.owner = owner;
        this.id = id;
        this.node_id = node_id;
        this.name = name;
        this.full_name = full_name;
        this.private_ = private_;
        this.description = description;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNode_id() {
        return node_id;
    }

    public void setNode_id(String node_id) {
        this.node_id = node_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public boolean isPrivate_() {
        return private_;
    }

    public void setPrivate_(boolean private_) {
        this.private_ = private_;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "SingleRepository{" +
                "id=" + id +
                ", node_id='" + node_id + '\'' +
                ", name='" + name + '\'' +
                ", full_name='" + full_name + '\'' +
                ", private_=" + private_ +
                ", description='" + description + '\'' +
                ", owner=" + owner +
                '}';
    }
}
