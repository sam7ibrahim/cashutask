package sam7ibrahim.cachutask.com.data.models.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import sam7ibrahim.cachutask.com.data.models.SingleRepository;

@Dao
public interface RepositoryDao {

    @Query("SELECT * FROM SingleRepository")
    List<SingleRepository> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveSingleRepository(SingleRepository singleRepository);

    @Query("DELETE FROM SingleRepository")
    void deleteAll();

}
