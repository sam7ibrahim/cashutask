/**
 * @file ApiEndPointInterface.java
 * @brief This class will have all constants related to the networking and retrofit instance
 * @author Shrikant
 * @date 15/04/2018
 */
package sam7ibrahim.cachutask.com.data.network;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import sam7ibrahim.cachutask.com.data.models.SingleRepository;

public interface ApiEndPointInterface {

    @GET("repos")
    Call<List<SingleRepository>> getApiResponse(@Query("page") int page, @Query("per_page") int per_page);

}
