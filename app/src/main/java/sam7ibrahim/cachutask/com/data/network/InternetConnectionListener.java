package sam7ibrahim.cachutask.com.data.network;

public interface InternetConnectionListener {

    void onInternetUnavailable();

    void onCacheUnavailable();
}
