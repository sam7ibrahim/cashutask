package sam7ibrahim.cachutask.com.data.repository.implementation;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sam7ibrahim.cachutask.com.data.models.SingleRepository;
import sam7ibrahim.cachutask.com.MyApplication;
import sam7ibrahim.cachutask.com.data.models.OnResult;
import sam7ibrahim.cachutask.com.data.network.ApiEndPointInterface;
import sam7ibrahim.cachutask.com.data.repository.repos.homeRepoistory;
import sam7ibrahim.cachutask.com.utils.AppExecutors;

public class homeRepositoryImp implements homeRepoistory {

    private ApiEndPointInterface apiEndPointInterface;

    public homeRepositoryImp() {
        this.apiEndPointInterface = MyApplication.getInstance().getApiEndPointInterface();
    }

    @Override
    public void getApiResponse(int page, int perPage, final OnResult onResult) {

        if (MyApplication.getInstance().isConnectedToInternet()) {
            getRepositoriesFromApi(page, perPage, onResult);
        }
        else {
            if(page > 1){
                onResult.onFailure(504,"No Internet Connection");
            }else {
                getRepositoriesFromLocalDataBase(onResult);
            }
        }

    }

    private void getRepositoriesFromLocalDataBase(OnResult onResult) {
        new AppExecutors().diskIO().execute(() -> {
            List<SingleRepository> repositories = MyApplication.getInstance().appDatabase.repositoryDao().getAll();
            if (null != repositories && repositories.size() > 0) {
                new AppExecutors().mainThread().execute(() -> onResult.onSuccess(repositories));
            } else {
                new AppExecutors().mainThread().execute(() -> onResult.onFailure(404, "no Repos Found"));
            }

        });

    }

    private void getRepositoriesFromApi(int page, int perPage, OnResult onResult) {
        Call<List<SingleRepository>> call =
                apiEndPointInterface.getApiResponse(page, perPage);

        call.enqueue(new Callback<List<SingleRepository>>() {
            @Override
            public void onResponse(Call<List<SingleRepository>> call,
                                   Response<List<SingleRepository>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    onResult.onSuccess(response.body());
                    saveRepositories(response.body(), page);
                } else {
                    try {
                        onResult.onFailure(response.code(), "error loading");
                    } catch (Exception e) {
                        onFailure(call, e);
                    }

                }
            }

            @Override
            public void onFailure(Call<List<SingleRepository>> call, Throwable t) {
                onResult.onFailure(t);
            }
        });
    }

    private void saveRepositories(List<SingleRepository> repositories, int page) {
        new AppExecutors().diskIO().execute(() -> {
            if (page == 1) {
                MyApplication.getInstance().appDatabase.repositoryDao().deleteAll();
            }
            for (SingleRepository repository : repositories) {
                MyApplication.getInstance().appDatabase.repositoryDao().saveSingleRepository(repository);
            }
        });
    }


}
