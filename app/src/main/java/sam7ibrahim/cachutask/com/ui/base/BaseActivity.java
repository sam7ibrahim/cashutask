package sam7ibrahim.cachutask.com.ui.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.LocaleList;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.blankj.utilcode.util.SnackbarUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;
import sam7ibrahim.cachutask.com.MyApplication;
import sam7ibrahim.cachutask.com.R;
import sam7ibrahim.cachutask.com.data.network.InternetConnectionListener;
import sam7ibrahim.cachutask.com.ui.base.presenter.PresenterFactory;
import sam7ibrahim.cachutask.com.ui.base.presenter.PresenterLoader;
import sam7ibrahim.cachutask.com.utils.views.progress.CustomProgress;

public abstract class BaseActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<BasePresenter>,
        BaseView, InternetConnectionListener {


    public static final String TAG = BaseActivity.class.getSimpleName();
    protected static final int LOADER_ID = 101;
    static boolean active = false;

    public CustomProgress progressDialog;
    protected View baseSnackBarview;
    protected RecyclerView rvItems;
    protected View tryAgain;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean isInternetConnected = true;

    @Nullable
    private BasePresenter presenter;

    private LocalBroadcastManager localBroadcastManager;

    public abstract PresenterFactory.PresenterType getPresenterType();

    public abstract void onPresenterAvailable(BasePresenter presenter);

    protected BasePresenter getPresenter() {
        return presenter;
    }


    @TargetApi(Build.VERSION_CODES.N)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        LocaleList localeList = new LocaleList(locale);
        LocaleList.setDefault(localeList);
        configuration.setLocales(localeList);
        return context.createConfigurationContext(configuration);
    }

    @SuppressWarnings("deprecation")
    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configLoader();
        setStatusBarcolor(R.color.colorPrimary);
        setLocaleLanguage("en");
        setUpInternetConnection();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupViews();
    }

    private void setUpInternetConnection() {
        ((MyApplication) getApplication()).setInternetConnectionListener(this);
    }


    private void configLoader() {
        progressDialog = CustomProgress.getInstance();

        getSupportLoaderManager().initLoader(LOADER_ID, null, this);
    }


    public void setStatusBarcolor(int colorId) {
        Window window = getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, colorId));

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int flags = window.getDecorView().getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            window.getDecorView().setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, colorId));
        }
    }


    @Override
    protected void onPause() {
        ((MyApplication) getApplication()).removeInternetConnectionListener();
        super.onPause();
    }


    public abstract void update(boolean isInternetConnection);

    @Override
    public Loader<BasePresenter> onCreateLoader(int i, Bundle bundle) {
        return new PresenterLoader(this, getPresenterType());
    }

    @Override
    public void onLoadFinished(Loader<BasePresenter> loader, final BasePresenter basePresenter) {
        presenter = basePresenter;
        Handler handler = new Handler();
        handler.post(() -> onPresenterAvailable(basePresenter));
    }

    @Override
    protected void onDestroy() {
        progressDialog.hideProgress();
        super.onDestroy();
    }

    @Override
    public void onLoaderReset(Loader<BasePresenter> loader) {
        this.presenter = null;
    }

    public void setLocaleLanguage(String language) {
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(language));
        res.updateConfiguration(conf, dm);
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }


    @Override
    protected void onStop() {
        active = false;
        super.onStop();
    }

    @Override
    public void showLoader() {
        if (!progressDialog.isShowing() && !this.isFinishing()) {
            progressDialog.showProgress(this, getString(R.string.loader), true);
        }
    }

    @Override
    public void hideLoader() {
        if (!BaseActivity.this.isFinishing() && null != progressDialog && progressDialog.isShowing()) {
            progressDialog.hideProgress();
        }
        stopSwipToRefreshLayoutRefreshing();
    }


    @Override
    public void showMessage(String message) {
        if (null == message) {
            message = getString(R.string.error_loading);
        }
        if (baseSnackBarview != null && isInternetConnected) {
            SnackbarUtils
                    .with(baseSnackBarview)
                    .setMessage(message)
                    .setDuration(SnackbarUtils.LENGTH_SHORT)
                    .showError();
        }
    }

    @Override
    public void showInternetConnectionError() {
        showMessage(getString(R.string.no_internet_connection));
    }

    @Override
    public void dismissAllSnackbars(){
        SnackbarUtils.dismiss();
    }

    public void showMessageWithActions(String message, String actionTitle, int length, View.OnClickListener onClickListener) {
        if (null == message) {
            message = getString(R.string.error_loading);
        }
        if (baseSnackBarview != null) {
            SnackbarUtils
                    .with(baseSnackBarview)
                    .setMessage(message)
                    .setDuration(length)
                    .setAction(actionTitle, onClickListener)
                    .showError();
        }
    }


    @Override
    public void showLoggedError(String message) {
        if (isInternetConnected) {
            showMessage(getString(R.string.error_loading));
            if (message != null) {
                Log.e(TAG, message);
            }
        }
    }



    protected void setBaseViews(View tryAgain, View snackBarview, RecyclerView RecyclerView) {
        this.baseSnackBarview = snackBarview;
        this.rvItems = RecyclerView;
        this.tryAgain = tryAgain;
    }

    protected void setBaseViews(View tryAgain, View snackBarview) {
        this.tryAgain = tryAgain;
        this.baseSnackBarview = snackBarview;
    }

    protected void setSwipeToRefresh(SwipeRefreshLayout swipeRefreshLayout) {
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    protected void stopSwipToRefreshLayoutRefreshing() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }


    @Override
    public void onInternetUnavailable() {
        if (!MyApplication.getInstance().isConnectedToInternet()){
            isInternetConnected = false;
            runOnUiThread(() -> showMessageWithActions(getString(R.string.no_internet_connection), getString(R.string.dismiss), SnackbarUtils.LENGTH_INDEFINITE, v -> SnackbarUtils.dismiss()));
        }
    }

    @Override
    public void onCacheUnavailable() {
        runOnUiThread(() -> showMessageWithActions(getString(R.string.no_internet_connection), getString(R.string.dismiss), SnackbarUtils.LENGTH_INDEFINITE, v -> SnackbarUtils.dismiss()));

    }

}
