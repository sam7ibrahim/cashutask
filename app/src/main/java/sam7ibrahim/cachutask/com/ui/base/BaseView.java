package sam7ibrahim.cachutask.com.ui.base;

public interface BaseView {

    void setupViews();

    void showLoader();

    void hideLoader();

    void showLoggedError(String message);

    void showMessage(String message);

    void showInternetConnectionError();

    void dismissAllSnackbars();
}
