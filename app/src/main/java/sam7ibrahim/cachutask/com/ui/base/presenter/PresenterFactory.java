package sam7ibrahim.cachutask.com.ui.base.presenter;

import android.content.Context;

import sam7ibrahim.cachutask.com.ui.base.BasePresenter;
import sam7ibrahim.cachutask.com.ui.home.HomePresenter;
import sam7ibrahim.cachutask.com.utils.Injector;

public class PresenterFactory {

    public static BasePresenter create(PresenterType type, Context context) {
        BasePresenter presenter = null;

        switch (type) {

            case HOME:
                presenter = new HomePresenter(Injector.provideHomeRepository());
                break;
            default:
                break;

        }

        return presenter;
    }

    public enum PresenterType {
        HOME
    }
}
