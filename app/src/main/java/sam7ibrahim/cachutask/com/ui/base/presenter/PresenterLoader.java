package sam7ibrahim.cachutask.com.ui.base.presenter;

import android.content.Context;

import androidx.loader.content.Loader;

import sam7ibrahim.cachutask.com.ui.base.BasePresenter;


public class PresenterLoader extends Loader<BasePresenter> {

    private PresenterFactory.PresenterType presenterType;
    private BasePresenter presenter;

    public PresenterLoader(Context context, PresenterFactory.PresenterType type) {
        super(context);
        presenterType = type;
    }

    @Override
    protected void onStartLoading() {

        // If we already own an instance, simply deliver it.
        if (presenter != null) {
            deliverResult(presenter);
            return;
        }

        // Otherwise, force a load
        forceLoad();
    }

    @Override
    protected void onForceLoad() {
        // Create the Presenter using the Factory
        presenter = PresenterFactory.create(presenterType, getContext());
        // Deliver the result
        deliverResult(presenter);
    }

    @Override
    protected void onReset() {
        presenter = null;
    }
}