package sam7ibrahim.cachutask.com.ui.home;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.List;

import sam7ibrahim.cachutask.com.R;
import sam7ibrahim.cachutask.com.data.models.SingleRepository;
import sam7ibrahim.cachutask.com.databinding.ActivityHomeBinding;
import sam7ibrahim.cachutask.com.ui.base.BaseActivity;
import sam7ibrahim.cachutask.com.ui.base.BasePresenter;
import sam7ibrahim.cachutask.com.ui.base.presenter.PresenterFactory;

public class HomeActivity extends BaseActivity implements HomePresenter.HomeView {

    private ActivityHomeBinding binding;
    private boolean loading = false;
    private boolean lastPage = false;
    private int currentPage = 1;
    public static Intent buildIntent(Context context) {
        return new Intent(context, HomeActivity.class);
    }

    public final static String TAG = HomeActivity.class.getSimpleName();
    private RepositoriesRecyclerViewAdapter repositoriesRecyclerViewAdapter;
    private HomePresenter mPresenter;


    @Override
    public PresenterFactory.PresenterType getPresenterType() {
        return PresenterFactory.PresenterType.HOME;
    }

    @Override
    public void onPresenterAvailable(BasePresenter presenter) {
        mPresenter = (HomePresenter) presenter;
        mPresenter.setView(this);
        getRepositories();
    }

    private void getRepositories() {
        onInternetUnavailable();
        mPresenter.getRepositories(currentPage,15);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        setBaseViews(binding.layoutTryAgain,binding.layoutContent,binding.repositoriesRecyclerView);
        setSwipeToRefresh(binding.strLayout);
        setUpRecyclerView();
        setupSwipeToRefresh();
    }

    private void setupSwipeToRefresh() {
        binding.strLayout.setOnRefreshListener(() -> {
            currentPage = 1;
            lastPage = false;
            getRepositories();
        });
    }

    private void setUpRecyclerView() {
        repositoriesRecyclerViewAdapter = new RepositoriesRecyclerViewAdapter(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        binding.repositoriesRecyclerView.setLayoutManager(layoutManager);
        binding.repositoriesRecyclerView.setAdapter(repositoriesRecyclerViewAdapter);
        binding.repositoriesRecyclerView.setNestedScrollingEnabled(false);

        binding.repositoriesRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                int itemsCount = layoutManager.getItemCount();
                int targetItem = ((LinearLayoutManager) layoutManager).findLastCompletelyVisibleItemPosition();

                Log.d(TAG, "item count : "+ itemsCount+" targetItem : "+ targetItem);

                if (itemsCount-targetItem<=4){
                    if (!loading && !lastPage && dy>0 ){
                        Log.d(TAG, "call api");
                        loading =true;
                        currentPage++;
                        getRepositories();
                    }
                }
            }
        });
    }

    @Override
    public void update(boolean isInternetConnection) {

    }

    @Override
    public void showRepositories(List<SingleRepository> repositories) {
        loading=false;
        if (null != repositories) {
            if(currentPage == 1){
                repositoriesRecyclerViewAdapter.clearData();
            }
            repositoriesRecyclerViewAdapter.addData(repositories);

            if (repositories.size() > 0) {
                if(repositories.size() < 15){
                    lastPage = true;
                    hideLoadMoreLoader();
                }


                binding.noData.setVisibility(View.GONE);
                binding.repositoriesRecyclerView.setVisibility(View.VISIBLE);
            } else {
                binding.noData.setVisibility(View.VISIBLE);
                binding.repositoriesRecyclerView.setVisibility(View.GONE);
            }
        } else {
            binding.noData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoadMoreLoader() {
        repositoriesRecyclerViewAdapter.removeLoader();
    }


    @Override
    public void setupViews() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.clearViews();
    }
}
