package sam7ibrahim.cachutask.com.ui.home;


import androidx.annotation.NonNull;



import java.lang.ref.WeakReference;
import java.util.List;

import sam7ibrahim.cachutask.com.data.models.SingleRepository;
import sam7ibrahim.cachutask.com.data.models.OnResult;
import sam7ibrahim.cachutask.com.data.repository.repos.homeRepoistory;
import sam7ibrahim.cachutask.com.ui.base.BasePresenter;
import sam7ibrahim.cachutask.com.ui.base.BaseView;



public class HomePresenter implements BasePresenter {

    private WeakReference<HomeView> weakView = new WeakReference<>(null);

    private homeRepoistory mHomeRepoistory;



    public HomePresenter(@NonNull homeRepoistory repository) {

        mHomeRepoistory = repository;

    }

    public void setView(HomeView view) {
        this.weakView = new WeakReference<>(view);
    }


    public void getRepositories(int page,int perPage) {
        mHomeRepoistory.getApiResponse(page,perPage, new OnResult<List<SingleRepository>>() {
            @Override
            public void onSuccess(List<SingleRepository> response) {
                final HomeView homeView = weakView.get();
                if (null != homeView) {
                    if (null != response) {
                        homeView.showRepositories(response);
                    } else {
                        homeView.showMessage("repositories not found");
                    }
                    homeView.hideLoader();
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                final HomeView homeView = weakView.get();
                if (null != homeView) {
                    homeView.hideLoader();
                    homeView.hideLoadMoreLoader();
                    homeView.showLoggedError(throwable.getMessage());
                }
            }

            @Override
            public void onFailure(int code, String message) {
                final HomeView homeView = weakView.get();
                if (null != homeView) {
                    homeView.hideLoader();
                    homeView.hideLoadMoreLoader();
                    switch (code) {
                        case 504:
                            homeView.showInternetConnectionError();
                            break;
                        default:
                            homeView.showMessage(message);
                            break;
                    }
                }
            }
        });
    }

    @Override
    public void clearViews() {
        this.weakView = new WeakReference<>(null);
    }


    public interface HomeView extends BaseView {
        void showRepositories(List<SingleRepository> repositories);
        void hideLoadMoreLoader();
    }

}
