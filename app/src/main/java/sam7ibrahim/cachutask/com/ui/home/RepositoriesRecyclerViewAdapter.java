package sam7ibrahim.cachutask.com.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import sam7ibrahim.cachutask.com.data.models.SingleRepository;
import sam7ibrahim.cachutask.com.R;

public class RepositoriesRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    Context mContext;
    List<SingleRepository> singleRepositories = new ArrayList<>();
    LoaderViewHolder loaderViewHolder;
    public RepositoriesRecyclerViewAdapter(Context mContext) {
        this.mContext = mContext;

    }
    public RepositoriesRecyclerViewAdapter() {

    }


    public void addData(List<SingleRepository> singleRepositories){
        this.singleRepositories.addAll( singleRepositories);
        notifyDataSetChanged();
    }

    public void clearData(){
        this.singleRepositories.clear();
        notifyDataSetChanged();
    }

    public static class OneItemviewholder extends RecyclerView.ViewHolder {
        public LinearLayout ll_container;
        public TextView tv_name, tv_description, tv_ownerType;
        public ImageView iv_ownerAvatar;

        public OneItemviewholder(LinearLayout ll_container, TextView tv_name, TextView tv_description, TextView tv_ownerType, ImageView iv_ownerAvatar) {
            super(ll_container);
            this.ll_container = ll_container;
            this.tv_name = tv_name;
            this.tv_description = tv_description;
            this.tv_ownerType = tv_ownerType;
            this.iv_ownerAvatar = iv_ownerAvatar;
        }
    }

    public static class LoaderViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout ll_container;
        public ProgressBar loader;
        public LoaderViewHolder(LinearLayout ll_container, ProgressBar loader) {
            super(ll_container);
            this.ll_container = ll_container;
            this.loader = loader;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout ll_container = null;
        RecyclerView.ViewHolder vh = null;

        if (viewType==0){
            ll_container = (LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.repository_item, parent, false);

            TextView tv_name, tv_description, tv_ownerType;
            ImageView iv_ownerAvatar;

            tv_name = ll_container.findViewById(R.id.tv_ownerName);
            tv_description = ll_container.findViewById(R.id.tv_ownerDescription);
            tv_ownerType = ll_container.findViewById(R.id.tv_watchsNumber);
            iv_ownerAvatar = ll_container.findViewById(R.id.iv_ownerAvatar);


             vh = new OneItemviewholder(ll_container,tv_name,tv_description,tv_ownerType,iv_ownerAvatar);
             return vh;
        }
        else{
            ll_container=(LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.loading_item, parent, false);

            ProgressBar pb_loader = ll_container.findViewById(R.id.pb_loader);
            loaderViewHolder = new LoaderViewHolder(ll_container,pb_loader);
            return loaderViewHolder;
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder.getItemViewType()==0){
            OneItemviewholder onItem = (OneItemviewholder) holder;

            onItem.tv_name.setText(mContext.getString(R.string.person_name,singleRepositories.get(position).getFull_name()));
            onItem.tv_description.setText(mContext.getString(R.string.person_description,singleRepositories.get(position).getDescription()));
            onItem.tv_ownerType.setText(mContext.getString(R.string.person_type,singleRepositories.get(position).getOwner().getType()));
            Glide.with(mContext)
                    .load(singleRepositories.get(position).getOwner().getAvatar_url())
                    .centerCrop()
                    .into(onItem.iv_ownerAvatar);
        }
    }

    @Override
    public int getItemCount() {
        return singleRepositories.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
       if (position==(singleRepositories.size()))
           // type for loader item
           return 1;
       else
           // type for normal item
           return 0;
    }

    public void removeLoader(){
        loaderViewHolder.loader.setVisibility(View.GONE);
    }

    public void showLoader(){
        loaderViewHolder.loader.setVisibility(View.VISIBLE);
    }
}