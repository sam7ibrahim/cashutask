package sam7ibrahim.cachutask.com.ui.splash;

import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import sam7ibrahim.cachutask.com.R;
import sam7ibrahim.cachutask.com.ui.home.HomeActivity;



public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_SCREEN_DELAY_TIME = 2000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        navigateToNextActivity();
    }

    private void navigateToNextActivity() {
        new Handler().postDelayed(() -> {

            startActivity(HomeActivity.buildIntent(this));

            finish();
        }, SPLASH_SCREEN_DELAY_TIME);
    }

}
