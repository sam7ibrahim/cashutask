package sam7ibrahim.cachutask.com.utils;


import sam7ibrahim.cachutask.com.data.repository.implementation.homeRepositoryImp;
import sam7ibrahim.cachutask.com.data.repository.repos.homeRepoistory;


public class Injector {

    public static homeRepoistory provideHomeRepository() {
        return new homeRepositoryImp();
    }

}
