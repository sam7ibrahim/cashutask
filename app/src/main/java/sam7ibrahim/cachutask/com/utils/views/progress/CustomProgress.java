package sam7ibrahim.cachutask.com.utils.views.progress;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.PorterDuff;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import sam7ibrahim.cachutask.com.R;


public class CustomProgress {

    public static CustomProgress customProgress = null;
    public Dialog mDialog;

    public static CustomProgress getInstance() {
        if (customProgress == null) {
            customProgress = new CustomProgress();
        }
        return customProgress;
    }

    public void showProgress(Activity context, String message, boolean cancelable) {
        mDialog = new Dialog(context);
        // no tile for the dialog
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.progress_bar_dialog);
        ProgressBar mProgressBar = mDialog.findViewById(R.id.progress_bar);
        mProgressBar.getIndeterminateDrawable().setColorFilter(context.getResources()
                .getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        TextView progressText = mDialog.findViewById(R.id.progress_text);
        progressText.setText("" + message);
        progressText.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
        mDialog.setCancelable(cancelable);
        mDialog.setCanceledOnTouchOutside(cancelable);
        mDialog.setOnCancelListener(dialog -> context.finish());
        mDialog.show();
    }

    public boolean isShowing() {
        return null != mDialog && mDialog.isShowing();
    }

    public void hideProgress() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}
