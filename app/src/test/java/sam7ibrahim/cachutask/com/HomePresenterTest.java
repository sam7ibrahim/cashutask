package sam7ibrahim.cachutask.com;

import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.ref.WeakReference;
import java.util.List;
import sam7ibrahim.cachutask.com.data.models.OnResult;
import sam7ibrahim.cachutask.com.data.models.SingleRepository;
import sam7ibrahim.cachutask.com.data.repository.repos.homeRepoistory;
import sam7ibrahim.cachutask.com.ui.home.HomePresenter;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;


public class HomePresenterTest {
    private HomePresenter homePresenter;
    private int pageNumber = 1;
    private int numPerPage = 15;

    @Mock
    private homeRepoistory repoistory;
    @Mock
    private List<SingleRepository> response;
    @Mock
    private WeakReference<HomePresenter.HomeView> weakView = new WeakReference<>(null);
    @Mock
    HomePresenter.HomeView homeView = weakView.get();
    @Captor
    private ArgumentCaptor<OnResult> onResultArgumentCaptor;

    @Before
    public void setUpHomePresenter(){
        MockitoAnnotations.initMocks(this);
        homePresenter = new HomePresenter(repoistory);
        homePresenter.setView(homeView);
    }

    @Test
    public void getRepositoriesTest(){
        homePresenter.getRepositories(pageNumber,numPerPage);
        verify(repoistory).getApiResponse(eq(pageNumber),eq(numPerPage),onResultArgumentCaptor.capture());
        //test success response
        onResultArgumentCaptor.getValue().onSuccess(response);
        verify(homeView).showRepositories(response);

        //test failure response
        onResultArgumentCaptor.getValue().onFailure(490,"unAthenticated");
        verify(homeView).showMessage("unAthenticated");

        //test failure response no internet connection
        onResultArgumentCaptor.getValue().onFailure(504,"No Internet Connection");
        verify(homeView).showInternetConnectionError();

        //test failure response
        Throwable throwable = new IllegalArgumentException();
        onResultArgumentCaptor.getValue().onFailure(throwable);
        verify(homeView).showInternetConnectionError();
        verify(homeView).showLoggedError(throwable.getMessage());

    }

    @Test
    public void clearViewTest(){
        homePresenter.clearViews();
    }

}